(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 320,
	height: 480,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/Bitmap1.png", id:"Bitmap1"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/Bitmap2.png", id:"Bitmap2"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/Bitmap5.png", id:"Bitmap5"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/Bitmap6.png", id:"Bitmap6"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/btn.jpg", id:"btn"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/img1.jpg", id:"img1"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/img2.jpg", id:"img2"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/mcrdslogo.png", id:"mcrdslogo"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_doktor/images/str_logo.png", id:"str_logo"}
	]
};



lib.ssMetadata = [];


lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Bitmap1 = function() {
	this.initialize(img.Bitmap1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,314,27);


(lib.Bitmap2 = function() {
	this.initialize(img.Bitmap2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,158,27);


(lib.Bitmap5 = function() {
	this.initialize(img.Bitmap5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,259,27);


(lib.Bitmap6 = function() {
	this.initialize(img.Bitmap6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,149,27);


(lib.btn = function() {
	this.initialize(img.btn);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,28);


(lib.img1 = function() {
	this.initialize(img.img1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.img2 = function() {
	this.initialize(img.img2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.mcrdslogo = function() {
	this.initialize(img.mcrdslogo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,85,22);


(lib.str_logo = function() {
	this.initialize(img.str_logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,46,46);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#33CCFF").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mcrdslogo();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85,22);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.btn();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,143,28);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap5();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,259,27);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap6();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,149,27);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,158,27);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap1();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,314,27);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.img2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.img1();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


// stage content:
(lib._320x480_doktor = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// ct
	this.instance = new lib.Symbol10();
	this.instance.setTransform(160,240,1.067,1.92,0,0,0,150,125);
	new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol10(), 3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

	// Layer 7
	this.instance_1 = new lib.Symbol6();
	this.instance_1.setTransform(-126,166.9,1,1,0,0,0,145,9.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(73).to({_off:false},0).to({x:156,alpha:1},15,cjs.Ease.get(0.5)).wait(138).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 8
	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(410.5,201.5,1.027,1.027,0,0,0,125.5,21.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(73).to({_off:false},0).to({x:140.5,alpha:1},15,cjs.Ease.get(0.5)).wait(138).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 6
	this.instance_3 = new lib.Symbol4();
	this.instance_3.setTransform(416,191.4,0.941,0.941,0,0,0,143,10.6);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:146,alpha:1},15,cjs.Ease.get(0.5)).wait(58).to({x:-134,alpha:0},12,cjs.Ease.get(-0.5)).to({_off:true},1).wait(153));

	// Layer 5
	this.instance_4 = new lib.Symbol3();
	this.instance_4.setTransform(-68.6,167.9,0.89,0.89,0,0,0,143.5,10);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:140,alpha:1},15,cjs.Ease.get(0.5)).wait(58).to({x:442.5,alpha:0},12,cjs.Ease.get(-0.5)).to({_off:true},1).wait(153));

	// Layer 15
	this.instance_5 = new lib.Symbol8();
	this.instance_5.setTransform(56,448,0.976,0.976,0,0,0,42.5,11);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(15).to({_off:false},0).to({alpha:1},12,cjs.Ease.get(0.5)).wait(199).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 9
	this.instance_6 = new lib.Symbol7();
	this.instance_6.setTransform(63,253,1,1,0,0,0,50,11);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(27).to({_off:false},0).to({y:233,alpha:1},11,cjs.Ease.get(0.5)).wait(188).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 10
	this.instance_7 = new lib.str_logo();
	this.instance_7.setTransform(247,419);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(239));

	// Layer 14
	this.instance_8 = new lib.Symbol2();
	this.instance_8.setTransform(160,79,1.123,1.123,0,0,0,150,58.6);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(88).to({_off:false},0).to({alpha:1},27,cjs.Ease.get(0.5)).wait(111).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 13
	this.instance_9 = new lib.Symbol1();
	this.instance_9.setTransform(160,-44.7,1.123,1.123,0,0,0,150,58.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({regY:58.6,y:79,alpha:1},15,cjs.Ease.get(0.5)).to({_off:true},100).wait(124));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgY/AlfMAAAhK+MAx/AAAMAAABK+g");
	this.shape.setTransform(160,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(239));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.4,129.6,626.6,590.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;