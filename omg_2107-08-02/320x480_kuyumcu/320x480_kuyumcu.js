(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {}; 

// library properties:
lib.properties = {
	width: 320,
	height: 480,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	webfonts: {},
	manifest: [
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/Bitmap1.png", id:"Bitmap1"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/Bitmap2.png", id:"Bitmap2"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/Bitmap4.png", id:"Bitmap4"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/btn.jpg", id:"btn"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/img11.jpg", id:"img11"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/img2.jpg", id:"img2"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/mcrdslogo.png", id:"mcrdslogo"},
		{src:"https://s3-eu-west-1.amazonaws.com/2017creatives/february/omg_2107-08-02/320x480_kuyumcu/images/str_logo.png", id:"str_logo"}
	]
};



lib.ssMetadata = [];


lib.webfontAvailable = function(family) { 
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Bitmap1 = function() {
	this.initialize(img.Bitmap1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,242,28);


(lib.Bitmap2 = function() {
	this.initialize(img.Bitmap2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,158,27);


(lib.Bitmap4 = function() {
	this.initialize(img.Bitmap4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,197,27);


(lib.btn = function() {
	this.initialize(img.btn);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,28);


(lib.img11 = function() {
	this.initialize(img.img11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.img2 = function() {
	this.initialize(img.img2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.mcrdslogo = function() {
	this.initialize(img.mcrdslogo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,85,22);


(lib.str_logo = function() {
	this.initialize(img.str_logo);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,46,46);


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#33CCFF").s().p("A3bTiMAAAgnDMAu2AAAMAAAAnDg");
	this.shape.setTransform(150,125);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(3).to({_off:false},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgGAIQgEgEAAgEQAAgEAEgDQADgDADAAQAEAAAEADQADAEAAADQAAAEgDAEQgEADgEAAQgDAAgDgDg");
	this.shape.setTransform(265.1,17.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgKA5IgWABIAAgEQAJgBACgCQACgDAAgHIAAhJQAAgKgBgDQgCgCgHAAIgCAAIAAgFQASgCAHgDIACAWQADgKAEgGQAGgFAIAAQAHAAAEAFQAFAFAAAIQAAAGgCADQgDAEgEAAQgIAAAAgIIAAgEIABgEQAAgFgFAAQgEAAgDACQgCADgDAFQgCAQAAANIAAAwQAAAHACADQABADAIAAIAAAEIgTgBg");
	this.shape_1.setTransform(259.2,12.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AAABOIgUABIAAgFQAIAAACgDQADgCAAgHIAAhLQAAgIgCgDQgDgDgIABIAAgFQAPgBAMgFIAABjQAAAHADACQACADAJAAIAAAFIgVgBgAgHg6QgEgDAAgGQAAgEAEgDQADgEAEAAQADAAAEAEQADADABAEQgBAGgDADQgEADgDABQgEgBgDgDg");
	this.shape_2.setTransform(252.5,10.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAABQIgUABIAAgEQAIgBACgCQADgDAAgHIAAh4QAAgJgCgCQgDgCgIAAIAAgFQAPgBAMgFIAACQQAAAHADADQADACAIABIAAAEIgVgBg");
	this.shape_3.setTransform(247,10.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AAABOIgUABIAAgFQAIAAACgDQADgCAAgHIAAhLQAAgIgCgDQgDgDgIABIAAgFQAQgBALgFIAABjQABAHACACQADADAIAAIAAAFIgVgBgAgIg6QgDgDAAgGQAAgEADgDQAEgEAEAAQADAAAEAEQAEADAAAEQAAAGgEADQgEADgDABQgEgBgEgDg");
	this.shape_4.setTransform(241.5,10.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgdBSIAAiKQAAgKgCgCQgCgCgJAAIAAgFQAQgBAOgFIAABBQAEgJAGgFQACgEAIAAQAIAAAHAFQAGAEAEAIQAFAIADAJQACAKAAANQAAANgCALQgDALgFAHQgFAIgGAFQgHAEgIAAQgHAAgFgFQgGgFgFgLIgIAVgAgBgXQgDADgEAHQgCAHgBAIIgBAYIABAXQABAJADAGQADAFADADQACADAEAAQAFAAAEgDQAEgDADgHQACgHABgKQACgJgBgNQAAgygTAAQgFAAgCAEg");
	this.shape_5.setTransform(233.4,10.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AATAqIAAgDQgEALgHAEQgGAFgHAAQgIAAgGgEQgGgDgEgGQgCgFgBgGIgCgPIAAg2QAAgJgCgCQgCgEgHABIgBAAIAAgEQAOgCAPgFIAABQQAAAPAEAGQAEAGAJAAQADAAADgCQAEgCACgEQADgEABgFIABgNIAAg0QAAgJgCgCQgCgEgHABIgCAAIAAgEQAPgBAPgGIAABfQAAAIACACQABACAIABIACAAIAAAEQgQACgLAFg");
	this.shape_6.setTransform(218.4,12.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgKA4QgHgEgFgIQgFgIgDgLQgCgLAAgMQAAgLACgMQADgLAFgIQAFgIAHgFQAHgDAHAAQAGAAAEABQAFADAEAGIAFgNIADAAIAAAoIgEAAQgDgQgFgIQgFgHgJAAQgEAAgDAEQgDADgEAHQgDAHgBAKQgBALAAALIABAVQACAJACAHQADAGAEADQACADAFAAQAFAAADgCQAEgCACgEIAEgKIADgNIAHAAQAAAJgDAIQgCAHgEAFQgEAFgFADQgGACgFAAQgHAAgHgEg");
	this.shape_7.setTransform(208.9,12.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AAyA5IgVABIAAgEQAHgBADgCQACgCAAgIIAAg9QAAgNgDgGQgEgGgIABQgJAAgEAGQgFAIAAANIAAA6QAAAIACACQADACAHABIAAAEIgUgBIgTABIAAgEQAHgBADgCQACgCAAgIIAAg8QAAgNgDgHQgEgFgIAAQgEAAgEACQgEADgCAEQgEAGAAAOIAAA4QAAAIACACQADACAHABIAAAEIgVgBIgVABIAAgEQAIgBACgCQADgDAAgHIAAhIQAAgLgCgBQgBgDgHAAIgDAAIAAgFQAQgCAKgFIACAVQAFgKAGgEQAGgFAJAAQAJAAAFAFQAEAFADAKQAFgKAGgFQAHgFAJAAQAHABAEACQAGACADAEQADAEABAIQACAGAAAKIAAA8QAAAHACADQADACAIABIAAAEIgVgBg");
	this.shape_8.setTransform(196.9,12.4);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AATAqIAAgDQgEALgHAEQgGAFgHAAQgIAAgGgEQgGgDgEgGQgCgFgBgGIgCgPIAAg2QAAgJgCgCQgCgEgHABIgBAAIAAgEQAOgCAPgFIAABQQAAAPAEAGQAEAGAJAAQADAAADgCQAEgCACgEQADgEABgFIABgNIAAg0QAAgJgCgCQgCgEgHABIgCAAIAAgEQAPgBAPgGIAABfQAAAIACACQABACAIABIACAAIAAAEQgQACgLAFg");
	this.shape_9.setTransform(183.7,12.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AghBQIAAgMIACAAQAIAAAGgFQAFgFAEgLIAGgSIgchcQgCgGgDgDQgDgBgHgBIAAgFIAVABIAUgBIAAAFQgKAAAAAFIABAGIARBBIAUg/IACgIQAAgFgLAAIAAgFIAPABIAQgBIAAAFQgGAAgDAEQgEACgCAFIgiB0IgFAMQgCAGgDADQgEADgEABQgFACgEAAg");
	this.shape_10.setTransform(173.7,14.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#FFFFFF").s().p("AATAqIAAgDQgEALgHAEQgGAFgHAAQgIAAgGgEQgGgDgEgGQgCgFgBgGIgCgPIAAg2QAAgJgCgCQgCgEgHABIgBAAIAAgEQAOgCAPgFIAABQQAAAPAEAGQAEAGAJAAQADAAADgCQAEgCACgEQADgEABgFIABgNIAAg0QAAgJgCgCQgCgEgHABIgCAAIAAgEQAPgBAPgGIAABfQAAAIACACQABACAIABIACAAIAAAEQgQACgLAFg");
	this.shape_11.setTransform(163.4,12.5);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#FFFFFF").s().p("AAiBRIgNAAIgag8IgMAQIAAAbQAAAHADADQACACAJABIAAAEIgWgBIgWABIAAgEQAJgBACgCQADgDAAgHIAAh3QAAgKgDgCQgCgCgJAAIAAgFQAQgBAOgFIAABqIAdgnQADgFAAgDQAAgFgLAAIAAgFIASABIATgBIAAAFQgHAAgEACQgEACgEAHIgTAXIAbA4QAFAIADACQADACAHABIAAAEg");
	this.shape_12.setTransform(153.1,10.1);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#FFFFFF").s().p("AgKA5IgWABIAAgEQAJgBACgCQACgDAAgHIAAhJQAAgKgBgDQgCgCgHAAIgCAAIAAgFQASgCAHgDIACAWQADgKAEgGQAGgFAIAAQAHAAAEAFQAFAFAAAIQAAAGgCADQgDAEgEAAQgIAAAAgIIAAgEIABgEQAAgFgFAAQgEAAgDACQgCADgDAFQgCAQAAANIAAAwQAAAHACADQABADAIAAIAAAEIgTgBg");
	this.shape_13.setTransform(139.2,12.4);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#FFFFFF").s().p("AgMA2QgHgEgFgHQgGgJgCgKQgDgMAAgMQAAgMADgLQADgKAEgJQAGgHAGgEQAHgEAHgBQAIAAAGAEQAHAEAEAHQAFAHACAKQACALABAOIg2AAIACAWQABAIADAHQACAHAFADQAFAFAEAAQAFAAAEgDQAEgCADgDQAEgFACgFQACgHAAgHIAIAAQgBAJgDAHQgCAIgFAFQgEAFgGADQgGADgHAAQgHAAgHgFgAATgJIgBgTQgCgHgCgGQgDgFgDgDQgDgCgFgBQgCAAgDADQgEADgCAFQgDAGgBAIIgCASIAkAAg");
	this.shape_14.setTransform(130.7,12.6);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#FFFFFF").s().p("AAaBQIgVABIAAgEQAHgBADgCQACgDAAgHIAAg8QAAgNgDgGQgEgHgJAAQgDAAgEADQgFACgCAFQgEAHAAAKIAAA7QAAAHADADQACACAIABIAAAEIgWgBIgUABIAAgEQAIgBACgCQADgDAAgHIAAh3QAAgKgDgCQgCgCgIAAIAAgFQAPgBAOgFIAAA/QAFgIAHgEQAFgEAGAAQAIAAAGAEQAFADAFAHIADALIABANIAAA7QAAAHACADQADACAIABIAAAEIgVgBg");
	this.shape_15.setTransform(120.9,10.1);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f("#FFFFFF").s().p("AgMAfQAHgIAEgLQADgLAAgGIAAAAIgCAAQgCAAgDgDQgCgEgBgFQAAgGADgDQAEgFADAAQAFABADAEQADAEAAAIQAAAKgEAKQgFAMgGANg");
	this.shape_16.setTransform(108.1,19.1);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f("#FFFFFF").s().p("AgKA5IgWABIAAgEQAJgBACgCQACgDAAgHIAAhJQAAgKgBgDQgCgCgHAAIgCAAIAAgFQASgCAHgDIACAWQADgKAEgGQAGgFAIAAQAHAAAEAFQAFAFAAAIQAAAGgCADQgDAEgEAAQgIAAAAgIIAAgEIABgEQAAgFgFAAQgEAAgDACQgCADgDAFQgCAQAAANIAAAwQAAAHACADQABADAIAAIAAAEIgTgBg");
	this.shape_17.setTransform(102.4,12.4);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f("#FFFFFF").s().p("AAABOIgUABIAAgFQAIAAACgDQADgCAAgHIAAhLQAAgIgCgDQgDgDgIABIAAgFQAQgBALgFIAABjQABAHACACQADADAIAAIAAAFIgVgBgAgIg6QgDgDAAgGQAAgEADgDQAEgEAEAAQADAAAEAEQAEADAAAEQAAAGgEADQgEADgDABQgEgBgEgDg");
	this.shape_18.setTransform(95.7,10.4);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f("#FFFFFF").s().p("AgVBOQgGgEgFgIQgFgIgCgLQgDgLAAgMQAAgNADgLQACgJAFgIQAEgIAHgEQAGgEAHAAQAIAAADAEQAFAEAFAJIAAgpQAAgJgCgDQgDgBgIAAIAAgFQAPgBAPgFIAACIQAAAKACAEQABACAIABIACAAIAAADQgSAEgIADIgDgTQgEAKgFAFQgFAEgHAAQgIAAgGgEgAgOgYQgDAEgDAGQgCAHgBAHIgCAXQAAANACAKQABAJACAIQADAGAEADQADAEAGgBQAEAAACgDQADgDACgGQADgGACgJIABgUIgBgZQgCgIgDgHQgCgIgDgDQgDgEgEABQgGAAgDACg");
	this.shape_19.setTransform(88.1,10.3);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f("#FFFFFF").s().p("AgMA2QgHgEgFgHQgGgJgCgKQgDgMAAgMQAAgMADgLQADgKAEgJQAGgHAGgEQAHgEAHgBQAIAAAGAEQAHAEAEAHQAFAHACAKQACALABAOIg2AAIACAWQABAIADAHQACAHAFADQAFAFAEAAQAFAAAEgDQAEgCADgDQAEgFACgFQACgHAAgHIAIAAQgBAJgDAHQgCAIgFAFQgEAFgGADQgGADgHAAQgHAAgHgFgAATgJIgBgTQgCgHgCgGQgDgFgDgDQgDgCgFgBQgCAAgDADQgEADgCAFQgDAGgBAIIgCASIAkAAg");
	this.shape_20.setTransform(78.3,12.6);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f("#FFFFFF").s().p("AAaA5IgUABIAAgEQAGgBADgCQADgCgBgIIAAg6QAAgPgDgGQgEgHgJABQgDAAgEACQgFADgBAEQgFAHAAAMIAAA5QABAIACACQADACAGABIAAAEIgUgBIgVABIAAgEQAIgBACgCQADgDAAgHIAAhIQAAgKgCgCQgBgDgIAAIgCAAIAAgFQAPgCALgFQABAIABAOQAFgKAGgFQAHgFAGAAQAHAAAHAEQAGAEADAGQADAEABAHIACAPIAAA5QgBAHACADQADACAIABIAAAEIgVgBg");
	this.shape_21.setTransform(68.5,12.4);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f("#FFFFFF").s().p("AAiBRIgNAAIgag8IgMAQIAAAbQAAAHADADQACACAJABIAAAEIgXgBIgVABIAAgEQAIgBADgCQACgDAAgHIAAh3QAAgKgCgCQgCgCgJAAIAAgFQAQgBAOgFIAABqIAdgnQADgFAAgDQAAgFgKAAIAAgFIARABIATgBIAAAFQgIAAgDACQgDACgFAHIgSAXIAbA4QADAIAEACQADACAHABIAAAEg");
	this.shape_22.setTransform(53,10.1);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f("#FFFFFF").s().p("AAABOIgUABIAAgFQAIAAADgDQACgCAAgHIAAhLQAAgIgCgDQgCgDgJABIAAgFQAQgBAMgFIAABjQgBAHADACQACADAJAAIAAAFIgVgBgAgHg6QgEgDAAgGQAAgEAEgDQADgEAEAAQAEAAADAEQADADAAAEQAAAGgDADQgDADgEABQgEgBgDgDg");
	this.shape_23.setTransform(44.9,10.4);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f("#FFFFFF").s().p("AAABQIgUABIAAgEQAIgBADgCQACgDAAgHIAAh4QAAgJgCgCQgCgCgJAAIAAgFQAQgBAMgFIAACQQAAAHACADQADACAIABIAAAEIgVgBg");
	this.shape_24.setTransform(39.4,10.1);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f("#FFFFFF").s().p("AgMA2QgHgEgFgHQgGgJgCgKQgDgMAAgMQAAgMADgLQADgKAEgJQAGgHAGgEQAHgEAHgBQAIAAAGAEQAHAEAEAHQAFAHACAKQACALABAOIg2AAIACAWQABAIADAHQACAHAFADQAFAFAEAAQAFAAAEgDQAEgCADgDQAEgFACgFQACgHAAgHIAIAAQgBAJgDAHQgCAIgFAFQgEAFgGADQgGADgHAAQgHAAgHgFgAATgJIgBgTQgCgHgCgGQgDgFgDgDQgDgCgFgBQgCAAgDADQgEADgCAFQgDAGgBAIIgCASIAkAAg");
	this.shape_25.setTransform(32.1,12.6);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f("#FFFFFF").s().p("AgKA4QgHgEgFgIQgFgIgDgLQgDgLAAgMQAAgLADgMQADgLAFgIQAFgIAHgFQAHgDAGAAQAHAAAFABQAEADAEAGIAFgNIADAAIAAAoIgEAAQgCgQgGgIQgFgHgIAAQgGAAgBAEQgFADgCAHQgDAHgCAKQgBALAAALIABAVQACAJADAHQACAGAEADQACADAGAAQAEAAADgCQADgCADgEIAFgKIACgNIAIAAQgBAJgDAIQgCAHgEAFQgEAFgFADQgFACgHAAQgGAAgHgEg");
	this.shape_26.setTransform(23.3,12.5);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f("#FFFFFF").s().p("AAaA5IgUABIAAgEQAGgBADgCQADgCgBgIIAAg6QAAgPgDgGQgEgHgJABQgDAAgEACQgEADgCAEQgFAHAAAMIAAA5QABAIACACQACACAIABIAAAEIgVgBIgVABIAAgEQAIgBACgCQADgDAAgHIAAhIQAAgKgCgCQgBgDgIAAIgCAAIAAgFQAQgCAKgFQACAIAAAOQAFgKAGgFQAHgFAGAAQAIAAAGAEQAGAEAEAGQACAEABAHIABAPIAAA5QAAAHACADQADACAIABIAAAEIgVgBg");
	this.shape_27.setTransform(13.7,12.4);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f("#FFFFFF").s().p("AAABjIgWACIAAgGQAJgBADgDQADgCgBgKIAAh0QABgJgDgDQgDgDgJgBIAAgGIAWACIAYgCIAAAGQgLABgCADQgCADgBAJIAAB0QABAKACACQACADALABIAAAGIgYgCgAgHhQQgDgDAAgFQAAgFADgDQAEgEADAAQAEAAAEAEQAEADAAAFQAAAFgEADQgEADgEAAQgDAAgEgDg");
	this.shape_28.setTransform(5.2,8.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15},{t:this.shape_14},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,270.9,28);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.mcrdslogo();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,85,22);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.btn();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,143,28);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap1();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,242,28);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap4();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,197,27);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Bitmap2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,158,27);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.img2();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.img11();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,300,117);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol9("synched",0);
	this.instance.setTransform(135.4,14,1,1,0,0,0,135.4,14);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,270.9,28);


// stage content:
(lib._320x480_kuyumcu = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// ct
	this.instance = new lib.Symbol10();
	this.instance.setTransform(160,240,1.067,1.92,0,0,0,150,125);
	new cjs.ButtonHelper(this.instance, 0, 1, 2, false, new lib.Symbol10(), 3);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(239));

	// Layer 7
	this.instance_1 = new lib.Symbol6();
	this.instance_1.setTransform(-126,166.9,1,1,0,0,0,145,9.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(73).to({_off:false},0).to({x:156,alpha:1},15,cjs.Ease.get(0.5)).wait(138).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 8
	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(410.5,201.5,1.027,1.027,0,0,0,125.5,21.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(73).to({_off:false},0).to({x:140.5,alpha:1},15,cjs.Ease.get(0.5)).wait(138).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 6
	this.instance_3 = new lib.Symbol4();
	this.instance_3.setTransform(416,191.4,0.941,0.941,0,0,0,143,10.6);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({x:146,alpha:1},15,cjs.Ease.get(0.5)).wait(58).to({x:-134,alpha:0},12,cjs.Ease.get(-0.5)).to({_off:true},1).wait(153));

	// Layer 5
	this.instance_4 = new lib.Symbol3();
	this.instance_4.setTransform(-68.6,167.9,0.89,0.89,0,0,0,143.5,10);
	this.instance_4.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({x:140,alpha:1},15,cjs.Ease.get(0.5)).wait(58).to({x:442.5,alpha:0},12,cjs.Ease.get(-0.5)).to({_off:true},1).wait(153));

	// Layer 15
	this.instance_5 = new lib.Symbol8();
	this.instance_5.setTransform(56,448,0.976,0.976,0,0,0,42.5,11);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(15).to({_off:false},0).to({alpha:1},12,cjs.Ease.get(0.5)).wait(199).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 9
	this.instance_6 = new lib.Symbol7();
	this.instance_6.setTransform(63,253,1,1,0,0,0,50,11);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(27).to({_off:false},0).to({y:233,alpha:1},11,cjs.Ease.get(0.5)).wait(188).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 10
	this.instance_7 = new lib.str_logo();
	this.instance_7.setTransform(247,419);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(239));

	// Layer 14
	this.instance_8 = new lib.Symbol2();
	this.instance_8.setTransform(160,79,1.123,1.123,0,0,0,150,58.6);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(88).to({_off:false},0).to({alpha:1},27,cjs.Ease.get(0.5)).wait(111).to({alpha:0},12,cjs.Ease.get(-0.5)).wait(1));

	// Layer 13
	this.instance_9 = new lib.Symbol1();
	this.instance_9.setTransform(160,-44.7,1.123,1.123,0,0,0,150,58.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({regY:58.6,y:79,alpha:1},15,cjs.Ease.get(0.5)).to({_off:true},100).wait(124));

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#000000").s().p("EgY/AlfMAAAhK+MAx/AAAMAAABK+g");
	this.shape.setTransform(160,240);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(239));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-36.4,129.6,626.6,590.5);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;