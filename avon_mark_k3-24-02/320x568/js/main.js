$(function(){
    $('.material').draggable({ revert: true });
    $('#face').droppable({
        drop : function(event, ui){
            $('#'+ui.draggable[0].id+'-on').show();
            checkAllVisible();
        }
    });
    
    $('#drag').addClass('fadeOut');
    
});

function checkAllVisible(){
    if( $('.on:visible').length === 4 ){
        setTimeout(function(){
            $('#wrapper').fadeOut();
            $('#finish').fadeIn();
        },1000);
    }
}