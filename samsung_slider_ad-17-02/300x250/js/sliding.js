$(document).ready(function () {

    var carouselMids = $("#mids");

    carouselMids.itemslide({
        disable_slide: false,
        duration: 1000
    });

    $(window).resize(function () {
        carouselMids.reload();
    });

    var static = document.getElementById('static');
    var hm = new Hammer(static);
    //hm.add(new Hammer.Pan({ threshold: 100 }))
    hm.on("swipeleft", function(ev) {
        carouselMids.next();
            
        return false;
    });

    hm.on("swiperight", function(ev) {
        carouselMids.previous();
            
        return false;
    });

});

