$(function(){
    
    var obj = {}
    
    $.ajax({
        type : 'GET',
        dataType : 'text',
        url : 'https://1hb6e8cjme.execute-api.eu-central-1.amazonaws.com/prod',
        success : function(response){
            response = response.replace(/\\/g, "");
            response = response.replace(/(?:\r\n|\r|\n)/g, '');
            response = response.replace(/"/g, "'");
            response = response.replace("'<", '"<').replace(">'", '>"');
            response = response.replace(/"/g, '');
            response = $.trim(response);
            response = $.parseXML(response);
            obj.sehir = $(response).find('weatherdata').children('location').children('name').text(); 
            obj.bugun = $(response).find('weatherdata').children('forecast').children('time').eq(0); 
            obj.yarin = $(response).find('weatherdata').children('forecast').children('time').eq(1); 
            obj.pazar = $(response).find('weatherdata').children('forecast').children('time').eq(2);            
            $('#tabs li a').click(function(){
                var date = $(this).data('href'); console.log(date);
                $('#location').html(obj.sehir + ' <span>'+ obj[date].find('temperature').attr('max') +'<sup>&deg;</sup></span>');
                $('#hissedilen span').html( obj[date].find('temperature').attr('day') + ' <sup>&deg;</sup>' );
                $('#nem span').html( obj[date].find('humidity').attr('value') );
                $('#ruzgar span').html( convertDirection(obj[date].find('windDirection').attr('code')) );
                $('#weather').attr('src', 'https://openweathermap.org/img/w/'+ obj[date].find('symbol').attr('var') +'.png');

                $(this).parent().addClass('on').siblings().removeClass('on');
            });
    
            $('#bugun a').trigger('click');
        },
        error : function(response){
            
        }
    });
});

function convertDirection(direction){
    if (direction == 'N') return 'Kuzey';
    if (direction == 'S') return 'Güney';
    if (direction == 'E') return 'Doğu';
    if (direction == 'W') return 'Batı';
    
    if (direction == 'NNE') return 'Kuzey-Kuzeydoğu';
    if (direction == 'NNW') return 'Kuzey-Kuzeybatı';
    if (direction == 'SSE') return 'Güney-Güneydoğu';
    if (direction == 'SSW') return 'Güney-Güneybatı';
    
    if (direction == 'ENE') return 'Doğu-Kuzeydoğu';
    if (direction == 'ENW') return 'Doğu-Kuzeybatı';
    if (direction == 'WSE') return 'Batı-Güneydoğu';
    if (direction == 'WSW') return 'Batı-Güneybatı';
    
    if (direction == 'NE') return 'Kuzeydoğu';
    if (direction == 'NW') return 'Kuzeybatı';
    if (direction == 'SE') return 'Güneydoğu';
    if (direction == 'SW') return 'Güneybatı';
}