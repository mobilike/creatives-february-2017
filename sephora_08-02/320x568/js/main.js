console.log("MAIN JS LOADED");
//Dependency & App Logic
requirejs.config({
    baseUrl: 'https://s3-eu-west-1.amazonaws.com/2017creatives/february/sephora_08-02/320x568/js/',
    paths: {
        mustache: 'mustache',
        gmaps: 'gmaps'
    }
});



// App logic.
requirejs(['mustache', 'gmaps'],
    function (Mustache, gmaps) {
        console.log("ready");

        var dataDove = [
        {
            "type" : "icon",
            "name": "Sephora Akasya AVM",
            "lng": "41.000860",
            "lat": "29.054587",
            "address" : "Ankara Devlet Yolu Haydarpaşa Yönü 4.km Çeçen Sokak Akasya Ticaret Merkezi Zemin Kat No: 204-205 Acıbadem, Üsküdar / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Akmerkez",
            "lng": "41.077277",
            "lat": "29.027285",
            "address" : "Nispetiye Cad. 34337 1. Kat Mağaza No: 306 - 307 Etiler /İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Aqua Florya AVM",
            "lng": "40.966024",
            "lat": "28.797430",
            "address" : "Şenlikköy Mahallesi Yeşilköy Halkalı Cad. No:93 Florya-İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Astoria",
            "lng": "41.068545",
            "lat": "29.006006",
            "address" : "Büyükdere Cad. No:127 Esentepe / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Buyaka AVM",
            "lng": "41.026552",
            "lat": "29.126459",
            "address" : "Fatih Sultan Mehmet Mahallesi, Balkan Caddesi, No: 56, Ümraniye / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Capacity AVM",
            "lng": "40.977599",
            "lat": "28.870988",
            "address" : "Zeytinlik Mah. Fişekhane Cad. No:7 34710 Bakırköy / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Demirören AVM",
            "lng": "41.034858",
            "lat": "28.980236",
            "address" : "Hüseyin Ağa Mah. İstiklal Cad. No:50-54 Beyoğlu / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora İstinye Park AVM",
            "lng": "41.110043",
            "lat": "29.032485",
            "address" : "Pınar Mah. İstinye Bayırı Cad. İstinye / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Maltepe Carrefour",
            "lng": "40.919285",
            "lat": "29.164007",
            "address" : "Cevizli Mah. Tugay Yolu No:67 Maltepe / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Marmara Forum",
            "lng": "40.996839",
            "lat": "28.886201",
            "address" : "Osmaniye Mah. Çobançesme Koşuyolu Bulvarı No:3 Bakırkoy / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Marmara Park",
            "lng": "41.009876",
            "lat": "28.658293",
            "address" : "Mevlana Mah. Çelebi Mehmet Cad. No:33 A Kat:3 No:346 Esenyurt / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Nişantaşı Sephora",
            "lng": "41.077407",
            "lat": "29.028309",
            "address" : "Harbiye Mah.Teşvikiye Cd. Çevre Apt No:35/1 Şişli / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Kanyon Sephora",
            "lng": "41.077433",
            "lat": "29.028330",
            "address" : "Büyükdere cd. No185 -2. Kat 34394 Levent Şişli / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Palladium AVM",
            "lng": "40.986126",
            "lat": "29.100155",
            "addess" : "Barbaros Mah. Halk Caddesi No:6 34746 Yenisahra-Kadıkoy / İstanbul"
        },{
            "type" : "icon",
            "name": "Sephora Ankamall AVM",
            "lng": "39.952101",
            "lat": "32.830955",
            "address" : "Konya Devlet Yolu Üzeri, Emniyet Sarayı Yanı No:2 / 242 Kat:1 No:6 06630 Akköprü / Ankara"
        },{
            "type" : "icon",
            "name": "Sephora Tunalı Hilmi AVM",
            "lng": "39.900416",
            "lat": "32.863793",
            "address" : "Kavaklıdere Mah. Tunalı Hilmi Cad. No:90 Çankaya / Ankara"
        },{
            "type" : "icon",
            "name": "Sephora Gordion AVM",
            "lng": "39.900932",
            "lat": "32.691396",
            "address" : "Ankaralılar Cad. No:2 06810 Ümitkoy Mah. Giriş Kat No:047 Ankara"
        },{
            "type" : "icon",
            "name": "Sephora Panora AVM",
            "lng": "39.848447",
            "lat": "32.832239",
            "address" : "Türkiye Ticaret Merkezi 14. Blok No:1 06459 Oran / Ankara"
        },{
            "type" : "icon",
            "name": "Sephora Cepa AVM",
            "lng": "39.910093",
            "lat": "32.778863",
            "address" : "Eskişehir yolu 7.km Mustafa Kemal Mah.2123. Sok. No:2 Zemin Kat  z-27 Nolu Alan Çankaya / Ankara"
        },{
            "type" : "icon",
            "name": "Sephora Agora AVM",
            "lng": "38.394622",
            "lat": "27.054104",
            "address" : "Mithatpaşa Cad. No:1446 35330 Balçova / İzmir"
        },{
            "type" : "icon",
            "name": "Sephora Alsancak",
            "lng": "38.431746",
            "lat": "27.139636",
            "address" : "Kültür Mah. 1382 Sok. No:18/A 35220 Konak / İzmir"
        },{
            "type" : "icon",
            "name": "Sephora Point Bornova",
            "lng": "38.430797",
            "lat": "27.210654",
            "address" : "4174 Sokak No:4 Bağburnu Mevkii Bornova / İzmir"
        },{
            "type" : "icon",
            "name": "Sephora Forum Bornova",
            "lng": "38.451116",
            "lat": "27.210030",
            "address" : "Kazım Dirim Mah. 372 Sok. No:38/G Bornova / İzmir"
        },{
            "type" : "icon",
            "name": "Sephora TerraCity AVM",
            "lng": "36.852483",
            "lat": "30.757336",
            "address" : "Çağlayan Mah. Tekelioğlu Cad. 1979 Sokak, Muratpaşa Belediyesi Yanı 07160 Lara / Antalya"
        },{
            "type" : "icon",
            "name": "Sephora Ziyapaşa",
            "lng": "37.000862",
            "lat": "35.320087",
            "address" : "Kurtuluş Mah. Ziyapaşa Bulv. No: 10/B Seyhan / Adana"
        },{
            "type" : "icon",
            "name": "Sephora Optimum",
            "lng": "36.991064",
            "lat": "35.340167",
            "address" : "Hacı Sabancı Blv. No:28/ZM01 Yüreğir / Adana"
        },{
            "type" : "icon",
            "name": "Sephora Mersin Forum",
            "lng": "36.784397",
            "lat": "34.588839",
            "address" : "Güvenevler Mh. Forum Avm 1 CD No:120/133 Yenişehir / Mersin"
        },{
            "type" : "icon",
            "name": "Sephora Korupark AVM",
            "lng": "40.250456",
            "lat": "28.958724",
            "address" : "Emek Adnan Menderes Mah. Turgut Özal Cad. Korupark Avm Sit. No:2/112 Osmangazi / Bursa"
        }],

        /* Info Window Template */
            dowTemplate = '<div class="infoWindow">' +
                '{{#.}}' +
                '<b>{{name}}</b><br/>' +
                '{{address}}' +
                '{{/.}}' +
                '</div>';

        /* Icons Types */
        var icons = {
            icon: {
                icon:'https://s3-eu-west-1.amazonaws.com/2017creatives/february/sephora_08-02/320x568/img/marker.png'
            }
        };

        /* Parameters : data, mapId, zoomLevel, template, direction(true,false), openMarkers (true or false), iconUrl, icons */
        new gmaps.DrawMap(dataDove, 'map-canvas', 12, dowTemplate, true, false, 'https://s3-eu-west-1.amazonaws.com/2017creatives/february/sephora_08-02/320x568/img/locator.png', icons);
        
        var mapShowClick = document.getElementById("btn"),
            mapWrapper = document.getElementById("mapWrapper"),
            closeMap = document.getElementById("closeMap");
        
        mapShowClick.addEventListener("click", function(){
            document.getElementById('Stage').style.display = "none";
        });

        closeMap.addEventListener("click", function(){
            document.getElementById('Stage').style.display = "block";
        });

    }
);