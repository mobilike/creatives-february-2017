console.log("MAIN JS LOADED");
//Dependency & App Logic
requirejs.config({
    baseUrl: 'https://s3-eu-west-1.amazonaws.com/2017creatives/february/halkbank-map_22-02/320x568/js/',
    paths: {
        mustache: 'mustache',
        gmaps: 'gmaps'
    }
});



// App logic.
requirejs(['mustache', 'gmaps'],
    function (Mustache, gmaps) {
        console.log("ready");

        var dataDove = [
        {
            "type" : "icon",
            "name": "Shell Gayrettepe",
            "lng": "41.060798",
            "lat": "29.0117134"
        },
        {
            "type" : "icon",
            "name": "Shell Teşvikiye",
            "lng": "41.0607976",
            "lat": "29.0051473"
        },
        {
            "type" : "icon",
            "name": "Shell Zincirlikuyu",
            "lng": "41.0607976",
            "lat": "29.0051473"
        },
        {
            "type" : "icon",
            "name": "Shell Levent",
            "lng": "41.087736",
            "lat": "29.0088945"
        },
        {
            "type" : "icon",
            "name": "Shell Bayrampaşa",
            "lng": "41.03962",
            "lat": "28.9026412"
        },
        {
            "type" : "icon",
            "name": "Shell Kadıköy",
            "lng": "41.0050546",
            "lat": "29.0220455"
        },
        {
            "type" : "icon",
            "name": "Shell Beyoğlu",
            "lng": "41.034622",
            "lat": "28.9594773"
        },
        {
            "type" : "icon",
            "name": "Shell Kağıthane",
            "lng": "41.084297",
            "lat": "28.9695173"
        },
        {
            "type" : "icon",
            "name": "Shell Sarıyer",
            "lng": "41.1177613",
            "lat": "29.0555455"
        },
        {
            "type" : "icon",
            "name": "Shell Fatih",
            "lng": "41.016699",
            "lat": "28.9463265"
        },
        {
            "type" : "icon",
            "name": "Shell Ayazağa",
            "lng": "41.11752",
            "lat": "29.0147653"
        },
        {
            "type" : "icon",
            "name": "Shell Şişli",
            "lng": "41.0607976",
            "lat": "29.0051473"
        }],

        /* Info Window Template */
            dowTemplate = '<div class="infoWindow">' +
                '{{#.}}' +
                '<b>{{name}}</b><br/>'+
                '{{/.}}' +
                '</div>';

        /* Icons Types */
        var icons = {
            icon: {
                icon:'https://s3-eu-west-1.amazonaws.com/2017creatives/february/halkbank-map_22-02/320x568/img/marker.png'
            }
        };

        /* Parameters : data, mapId, zoomLevel, template, direction(true,false), openMarkers (true or false), iconUrl, icons */
        new gmaps.DrawMap(dataDove, 'map-canvas', 12, dowTemplate, true, false, 'https://s3-eu-west-1.amazonaws.com/2017creatives/february/halkbank-map_22-02/320x568/img/locator.png', icons);
        
        var mapShowClick = document.getElementById("btn"),
            mapWrapper = document.getElementById("mapWrapper"),
            closeMap = document.getElementById("closeMap");
        
        mapShowClick.addEventListener("click", function(){
            document.getElementById('Stage').style.display = "none";
        });

        closeMap.addEventListener("click", function(){
            document.getElementById('Stage').style.display = "block";
        });

    }
);